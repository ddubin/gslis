#!/usr/bin/python

import  cgi, smtplib, string, sys, time, re, string
from rdflib import *


def notfound():
  print "Content-type: text/html\n\n"
  print '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">'
  print '<html><head>'
  print '<title>404 Not Found</title>'
  print '</head><body>'
  print '<h1>Not Found</h1>'
  print '<p>The requested URL was not found on this server.</p>'
  print '<hr>'
  print '</body></html>'


def is_uri(thing):                    # Boolean test if rdf term is a URI ref
  return(type(thing) == URIRef)

def is_literal(thing):                # Boolean test if rdf term is an rdf literal
  return(type(thing) == Literal)

def is_bnode(thing):
  return(type(thing) == BNode)

def setupgraph(mygraph):
  mygraph.bind("skos","http://www.w3.org/2004/02/skos/core#")
  mygraph.bind("foaf","http://xmlns.com/foaf/0.1/")
  mygraph.bind("dc","http://purl.org/dc/elements/1.1/")
  mygraph.bind("dcterms","http://purl.org/dc/terms/")
  mygraph.bind("xsd","http://www.w3.org/2001/XMLSchema#")
  mygraph.bind("sam","http://cirssweb.lis.illinois.edu/DataCon/SAM/")
  mygraph.bind("org","http://www.w3.org/ns/org#")
  mygraph.bind("gslis","http://courseweb.lis.illinois.edu/lis/2015sp/lis590odl/gslis/", override=True)


provig = Namespace("http://purl.org/RDA-Provenance/Concepts/")
skos = Namespace("http://www.w3.org/2004/02/skos/core#")
foaf = Namespace("http://xmlns.com/foaf/0.1/")
dc = Namespace("http://purl.org/dc/elements/1.1/")
dcterms = Namespace("http://purl.org/dc/terms/")
xsd = Namespace("http://www.w3.org/2001/XMLSchema#")
org = Namespace("http://www.w3.org/ns/org#")
sam = Namespace("http://cirssweb.lis.illinois.edu/DataCon/SAM/")
gslis = Namespace("http://courseweb.lis.illinois.edu/lis/2015sp/lis590odl/gslis/")

mygraph = ConjunctiveGraph()
mygraph.parse("gslis.ttl",format="n3")

inputform = cgi.FieldStorage()

repString = r'<?xml version="1.0" encoding="utf-8"?>\n<?xml-stylesheet href="http://courseweb.lis.illinois.edu/lis/2015sp/lis590odl/RDF/owl2xhtml.xsl" type="text/xsl"?>'
pattern = re.compile(r'<\?xml version="1.0" encoding="utf-8"\?>', re.MULTILINE)
rep2 = r'UTF-20"?>'
pattern2 = re.compile(r'UTF-8"\?>', re.MULTILINE)

if ('format' in inputform.keys()):
 f = inputform['format'].value
 outputgraph = mygraph

 if f == 'html':
   docstring = outputgraph.serialize(format="pretty-xml")   
   newstring = pattern.sub(repString, docstring, count=1)
   print "Content-type: application/rdf+xml\n"
   print newstring

 elif f == 'rdf':
   print "Content-type: application/rdf+xml\n"
   print outputgraph.serialize(format="xml")   
 else: notfound()
else: notfound()

